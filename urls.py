from django.conf.urls.defaults import *
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    (r'', include('like_button.urls')),
    (r'^', include('index.urls')),
    (r'^catalog/', include('catalog.urls')),
    (r'^contacto/', include('contact_form.urls')),
    (r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    # urlpatterns +=  (url(r'^rosetta/', include('rosetta.urls')),)
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

