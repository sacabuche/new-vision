import os
import sys
import django

sys.path.append('/home/daniel/django/projects/')
sys.path.append('/home/daniel/django/apps/')
sys.path.append('/home/daniel/django/projects/newvision/')


os.environ['DJANGO_SETTINGS_MODULE'] = 'newvision.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

