$(document).ready(function(){
  adjust_window();
  init_events();
  set_current_page()
});

var LOADING_URL = '/static/images/loading.gif';

function init_events(){
	$('#images-small img').live('click', load_image);
	$('#image-loader').hide();
	/* Validate if has support of history.pushState */

	var popped = ('state' in window.history)
		initialURL = location.href;


	if (history && history.pushState){
		$(window).bind('popstate', function(){
			var initialPop = !popped && location.href == initialURL;
			if ( initialPop ){
				return
			}
			slideTo(location.pathname);
		});
		$('#navigation a').click(push_state);
	}
	$('#big-image').live('click', big_image_click);
}


function adjust_window(){
  var wrapper = $('#vertical-wrapper');
  var content_heigth = $('#content').height();
 
  function adjust(){
    var doc_heigth = $(window).height() - content_heigth;
    var v_margin = doc_heigth/2;
    if (v_margin <= 0){
      $(wrapper).css('margin', "0 0");
    }
    else{
      $(wrapper).css('margin', v_margin + "px 0");
    }
  }
	
  adjust();
  $(window).resize(adjust);
}

/* Slider */
function push_state(e){
	/* Prevent default change of url */
	e.preventDefault();
	$a = $(e.target);

	if ($a.hasClass('current')){
		return
	}

	/* $(e.target).closest('ul').find('a').removeClass('current');
	//$(e.target).addClass('current'); */
	history.pushState({ path: this.path }, '', this.href);
	slideTo(this.href);
}

function replace_content(data){
	$('#square-content')
		.html(data)
		.removeClass('sliding');
}

function slideTo(path){
	/* Set color to link */
	var $navbar = $('#navigation');
	$navbar.find('a')
		.removeClass('current');

	$navbar.find('a[href="'+ location.pathname +'"]')
		.addClass('current');

	$('#square-content').addClass('sliding');
	var query = {
		url: path,
		success: replace_content,
	}
 	$.ajax(query);
}
/* End Slider */

function big_image_click(e){
	var $big_image = $(e.target),
	    index = $big_image.attr('data-index'),
	    is_last = $big_image.attr('data-is-last');

	if (is_last==="True"){
		index = 0
	}
	else{
		index = parseInt(index, 10) + 1;
	}
	var selection = '#images-small img[data-index='+index+']';
	var $image_selected = $(selection).click();

}

function load_image(e){
	var $image = $(e.target),
	    src = $image.attr('data-big-image'),
	    margin = $image.attr('data-image-margin'),
	    index = $image.attr('data-index'),
	    is_last = $image.attr('data-is-last');

	var $image_loader = $('#image-loader');

	if ($image_loader.attr('src') == src){
		return
	}

	$('#big-image')
		.attr('src', LOADING_URL)
		.css('margin', '')
		.addClass('loading');
	
	/* Preload the image in a hiddden container */
	$image_loader
		.attr('src', src)
		.load(function(){
			$('#big-image')
			    .removeClass('loading')
			    .css('margin', margin)
			    .attr({'src':src,
				    'data-index':index,
				    'data-is-last':is_last
			    });
		});
}

function set_current_page(){
  var current_path = location.pathname;
  $('a[href="'+current_path+'"]').addClass('current');
}

