function simplecarousel($obj){
	var LEFT = ACTIVE = 0,
	    RIGHT = INACTIVE = 1,

	    /* FIXME: to use multiple simplecarousel for page this must be moved */
	    IMAGES_TO_SCROLL = $obj.attr('data-to-scroll') || 11,
	    IMAGES_DISPLAYING = $obj.attr('data-displaying') || 11,
	    SHOW_TEXT = $obj.attr('data-show-text') || true,
	    /* FIXME  END*/
	    INNER_WRAPPER_NAME = 'simple-carousel-inner-wrapper',
	    EMPTY_IMAGE = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
	    $background,
            $b_left_arrow, $b_right_arrow,
	    background_img,
            $index_position,
	    $text, $close_text,
	    timer;

	IMAGE_TO_SCROLL = parseInt(IMAGES_TO_SCROLL, 10);
	IMAGES_DISPLAYING = parseInt(IMAGES_DISPLAYING, 10);


	function change_arrow_status(position, stat){
		if (position == LEFT){
			var $arrow = $('.simple-carousel-left-arrow');
		}
		else{ /* RIGHT */
			var $arrow = $('.simple-carousel-right-arrow');
		}


		if (stat == ACTIVE){
			$arrow.removeClass('disabled');
		}
		else{
			$arrow.addClass('disabled');
		}
	}

	var can_click = true;
	function on_click_arrow(e){
		if (!can_click){
			return
		}
		can_click = false;
		
		var data = e.data;
		var scroller = data.to_scroll,
		    direction = data.direction,
		    image_width = data.image_width,
		    posible_offset = data.posible_offset,
		    offset,
		    position;
		
		position = scroller.scrollLeft();
		offset = (image_width * IMAGES_TO_SCROLL);
		if (direction == LEFT){
			position -= offset;
		}
		else{
			position += offset;
		}
		
		/*Change the direction of the autoslide*/
		autoslide_dir = direction;
		/*This is necessary for adjusting the speed of scrollLeft*/

		/*  FIXME: this is to much else if*/
		if (position >= posible_offset){
			position = posible_offset;
			autoslide_dir = LEFT;
			/* disable arrow */
			change_arrow_status(RIGHT, INACTIVE);
		}
		else{
			change_arrow_status(RIGHT, ACTIVE);
		}

		if (position <= 0){
			position = 0;
			autoslide_dir = RIGHT;
			/* disable arrow */
			change_arrow_status(LEFT, INACTIVE);
		}
		else{
			change_arrow_status(LEFT, ACTIVE);
		}

		scroller.animate({'scrollLeft':position}, function(){
			can_click = true;
		});

		window.clearInterval(timer);
	};

	function set_arrow(obj, classes, direction, options){
		var the_options = jQuery.extend( true, {}, options);
		the_options.direction = direction;
		$(obj).addClass(classes).
		       click(the_options, on_click_arrow);
	};

	function create_arrows($wrapper, $inner_wrapper, image_width, total_width){
		var left_arrow = document.createElement('div');
		var right_arrow = document.createElement('div');

		/* No Arrows to set */
		if ($inner_wrapper.width() >= total_width){
			return;
		}

		var posible_offset = total_width - image_width * IMAGES_DISPLAYING;

		/*Set Events for click on arrows*/
		var options = {
			'to_scroll':$inner_wrapper,
			'image_width': image_width,
			'total_width': total_width,
			'posible_offset': posible_offset,
			'arrows': {
				'left': left_arrow,
				'right': right_arrow
			}};

		set_arrow(left_arrow,
			   'simple-carousel-left-arrow simple-carousel-arrow',
			   LEFT,
			   options);
		set_arrow(right_arrow,
			  'simple-carousel-right-arrow simple-carousel-arrow',
			  RIGHT,
			  options);

		/*Add the arrows for scroll*/
		$wrapper.append(left_arrow).append(right_arrow);
		change_arrow_status(LEFT, INACTIVE);

	};

	function adjust_width(img, img_q){
		var total_width,
		    image_width;
		    /*get necesary objects*/
		    $li = $(img).closest('li'),
		    $ul = $li.closest('ul');

		/*calculate new width*/
		image_width = $li.outerWidth();
		total_width = image_width * img_q;
		/*set new width*/
		$ul.width(total_width);
		return {'image':image_width, 'total':total_width}
	};

        
        /* background arrows */
        $b_left_arrow = $(document.createElement('div')).
                        attr('id', 'simple-carousel-b-left');
        $b_right_arrow = $(document.createElement('div')).
                        attr('id', 'simple-carousel-b-right');
        $(document.body).append($b_left_arrow);
        $(document.body).append($b_right_arrow);

        $b_right_arrow.click(function(e){
             next_big_image();
        });

        $b_left_arrow.click(function(e){
             next_big_image(-1);
        });


	$obj.each(function(){
		var $images,
		    $inner_wrapper,
	            $wrapper;


		/* create the super wrapper where arrows will be added */
		$(this).wrap('<div class="simple-carousel-wrapper" />');

		/* add wrapper where will scroll */
		
		$inner_wrapper = $(this).
		 		      wrap('<div class="'+INNER_WRAPPER_NAME+'" />').
		                      closest('.'+INNER_WRAPPER_NAME);

		/*Set thw width of inner Wrapper and create the arrows*/
		$wrapper = $(this).closest('.simple-carousel-wrapper');
		/* just select the photos that are visible (just one per group) 
		 * when grouping set a class named='simple-carousel-hidden' 
		 * this is needed because in adjust_width we pass the size,
		 * there shuld be a better way to do this an elegant one*/
		$images = $(this).find('li:not(.simple-carousel-hidden)').find('img');
		var img = $($images)[0];

		/*Verify if is on cache or already loaded*/
		if (img.complete){
			var width = adjust_width(img, $images.size());
			create_arrows($wrapper, $inner_wrapper,
					width.image, width.total);
		}
		/*Wait to load for execute the code*/
		else{
			$(img).load(function(){
				var width = adjust_width(img, $images.size());
				create_arrows($wrapper, $inner_wrapper,
						width.image, width.total);
			});
		}


	});

}

$(document).ready(function(){
    simplecarousel($('#carousel'));
})

