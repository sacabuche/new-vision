from django.contrib import admin
from catalog.models import Background, Catalog, CatalogImage

class CatalogImageInline(admin.StackedInline):
    model = CatalogImage
    extra = 0

class CatalogAdmin(admin.ModelAdmin):
    inlines = (CatalogImageInline,)

admin.site.register(Catalog, CatalogAdmin)
admin.site.register(Background)

