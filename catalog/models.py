from django.db import models
from django.utils.translation import gettext_lazy as _
from django.template.defaultfilters import slugify

class Background(models.Model):
    name= models.CharField(_('name'), max_length=50, unique=True)
    image = models.ImageField(_('image'), upload_to="uploads/backgrounds/")

    def __unicode__(self):
        return self.name

    def get_url(self):
        return self.image.url


class Catalog(models.Model):
    name = models.CharField(_('name'), max_length=50, unique=True)
    position = models.SmallIntegerField(_('position'), default=0)
    slug = models.CharField(max_length=50, editable=False, unique=True)
    date_time = models.DateTimeField(auto_now=True, editable=False)
    background = models.ForeignKey('Background', verbose_name=_('background'), null=True)
    description = models.TextField(_('description'), blank=True)

    class Meta:
        ordering = ('position','name')

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_url(self):
        return('catalog', [self.slug])

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Catalog, self).save(*args, **kwargs)


class CatalogImage(models.Model):
    catalog = models.ForeignKey('Catalog', related_name='images')
    image = models.ImageField(_('image'), upload_to="uploads/catalog_images/")
    group = models.CharField(_('group'), max_length=50, blank=True)
    position = models.SmallIntegerField(_('position'), default=0)
    description = models.TextField(_('description'), blank=True)

    class Meta:
        ordering = ('group','position')

    def __unicode__(self):
        return self.image.name

    def get_url(self):
        return self.image.url
