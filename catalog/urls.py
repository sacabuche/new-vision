from django.conf.urls.defaults import *
from django.contrib import admin
from catalog.views import CatalogDetailView


admin.autodiscover()

urlpatterns = patterns('core.views',
    # Example:
    url(r'^(?P<slug>[-\w]+)/$', CatalogDetailView.as_view(), name='catalog'),
)
