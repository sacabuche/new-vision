from django.views.generic import TemplateView



class IndexView(TemplateView):
    template_name='index/index.html'

    #class Background:
    #    image = '/media/images/index.jpg'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context.update({
            'index': True,
        })
        return context
